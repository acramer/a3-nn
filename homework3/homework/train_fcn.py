import torch
import numpy as np

from .models import FCN, save_model
from .utils import load_dense_data, DENSE_CLASS_DISTRIBUTION, ConfusionMatrix
from . import dense_transforms
import torch.utils.tensorboard as tb
from os import walk


def train(args):
    from os import path
    model = FCN(args.channels)
    train_logger, valid_logger = None, None
    folder_nums = [-1]
    if args.log_dir is not None:
        folder_nums = list(map(int, list(map(lambda x: x.split('-')[0], list(walk(args.log_dir))[0][1]))))
        folder_nums.sort()
        folder_nums.insert(0, -1)
        if args.description != '': description = '/' + str(folder_nums[len(folder_nums)-1] + 1) + '-f-' + args.description
        else: description = '/' + str(folder_nums[-1] + 1) + '-f'
        train_logger = tb.SummaryWriter(path.join((args.log_dir + description), 'train'), flush_secs=1)
        valid_logger = tb.SummaryWriter(path.join((args.log_dir + description), 'valid'), flush_secs=1)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    best_valid_accuracy = 0

    if args.continue_training:
        model.load_state_dict(torch.load(path.join(path.dirname(path.abspath(__file__)), 'fcn.th')))

    train_augmentations = dense_transforms.ToTensor()
    if args.data_aug:
        train_augmentations = dense_transforms.Compose([
            #dense_transforms.RandomResizedCrop((96, 128)),
            dense_transforms.RandomResizedCrop(64),
            dense_transforms.ToTensor()
        ])

    train_dl = load_dense_data(args.training_path, batch_size=args.batch_size)
    valid_dl = load_dense_data(args.validation_path, batch_size=args.batch_size)
    loss_func = torch.nn.CrossEntropyLoss()

    if args.adam:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)
    else:
        optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay, momentum=0.9)
    if args.step_schedule: scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', patience=5)

    global_step = 0
    for i in range(args.epochs):
        total_loss = 0
        train_confusion = ConfusionMatrix()
        valid_confusion = ConfusionMatrix()
        # Training
        model.train()
        for j, data in enumerate(train_dl):
            input, gold = data

            input = input.to(device)
            gold = gold.to(device)
            optimizer.zero_grad()
            loss = loss_func(model(input).to(device), gold.long())
            loss.backward()
            optimizer.step()

            # Log Loss
            if args.log_dir is not None:
                train_logger.add_scalar('loss', loss, global_step)

            total_loss += loss.item()
            ## Testing Accuracy
            train_confusion.add(model(input).argmax(1), gold.to(device))
            global_step += 1

        # Evaluation
        ## Validation Accuracy
        model.eval()
        for j, data in enumerate(valid_dl):
            input, gold = data
            valid_confusion.add(model(input.to(device)).argmax(1), gold.to(device))

        # Display Results
        # Log Accuracies
        if args.log_dir is not None:
            train_logger.add_scalar('accuracy/fcn', train_confusion.global_accuracy, global_step)
            valid_logger.add_scalar('accuracy/fcn', valid_confusion.global_accuracy, global_step)
            train_logger.add_scalar('iou', train_confusion.iou, global_step)
            valid_logger.add_scalar('iou', valid_confusion.iou, global_step)
        else: print("Epoch: {:3d} -- Training Loss: {:10.6f} | Training Accuracy {:7.3f}% | Training IoU {:7.3f} | Validation Accuracy {:7.3f}% | Validation IoU {:7.3f}".format(i, loss.item(), train_confusion.global_accuracy, train_confusion.iou, valid_confusion.global_accuracy, valid_confusion.iou))

        # Step Learning Rate
        if args.step_schedule:
            scheduler.step(valid_confusion.global_accuracy)
            train_logger.add_scalar('learning rate', optimizer.param_groups[0]['lr'], global_step)

        # Checkpoint Saving
        if args.checkpoints and valid_confusion.global_accuracy > best_valid_accuracy:
            best_valid_accuracy = valid_confusion.global_accuracy
            save_model(model, str(folder_nums[-1]+1))

    print_args(args)
    print("Saved: ", not args.no_save)
    if not args.no_save: save_model(model)

def print_args(args):
    print("\t-----------------------------------------------------------------------------")
    print("\t|  Option Descriptions  |       Option Strings       |     Default Values    ")
    print("\t-----------------------------------------------------------------------------")
    print("\t|Epochs:                | '-e', '--epochs'           |  ", args.epochs)
    print("\t|Learning Rate:         | '-l', '--learning_rate'    |  ", args.learning_rate)
    print("\t|Weight Decay:          | '-w', '--weight_decay'     |  ", args.weight_decay)
    print("\t|Batch Size:            | '-b', '--batch_size'       |  ", args.batch_size)
    print("\t|Channels Sizes:        | '-c', '--channels'         |  ", args.channels)
    print("\t|Kernal Size:           | '-k', '--k_size'           |  ", args.k_size)
    print("\t|Stride:                | '-s', '--stride'           |  ", args.stride)
    print("\t|Training path:         | '--training_path'          |  ", args.training_path)
    print("\t|Validation path:       | '--validation_path'        |  ", args.validation_path)
    print("\t|Step Schedule:         | '--step_schedule'          |  ", args.step_schedule)
    print("\t|Description:           | '--description'            |  ", args.description)
    print("\t|No Save:               | '--no_save'                |  ", args.no_save)
    print("\t|Continue Training:     | '--continue_training'      |  ", args.continue_training)
    print("\t|Checkpoints:           | '--checkpoints'            |  ", args.checkpoints)
    print("\t|Data Augmentation:     | '--data_aug'               |  ", args.data_aug)
    print("\t|Adam Optimizer:        | '--adam'                   |  ", args.adam)
    print("\t-----------------------------------------------------------------------------")


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument('--log_dir')
    # Put custom arguments here
    parser.add_argument('-e', '--epochs', type=int, default=600)
    parser.add_argument('-c', '--channels', type=str, default="16 24 32")
    parser.add_argument('-l', '--learning_rate', type=float, default=1e-3)
    parser.add_argument('-w', '--weight_decay', type=float, default=1e-4)
    parser.add_argument('-b', '--batch_size', type=int, default=200)
    parser.add_argument('-k', '--k_size', type=int, default=5)
    parser.add_argument('-s', '--stride', type=int, default=1)
    parser.add_argument('--training_path', type=str, default='dense_data/train/')
    parser.add_argument('--validation_path', type=str, default='dense_data/valid/')
    parser.add_argument('--step_schedule', action='store_true', default=False)
    parser.add_argument('--description', type=str, default='')
    parser.add_argument('--no_save', action='store_true', default=False)
    parser.add_argument('--continue_training', action='store_true', default=False)
    parser.add_argument('--checkpoints', action='store_true', default=False)
    parser.add_argument('--data_aug', action='store_true', default=False)
    parser.add_argument('--adam', action='store_true', default=False)
    parser.add_argument('-h', '--help', action='store_true', default=False)

    args = parser.parse_args()

    if args.help:
        print_args(args)
        exit()

    train(args)

from .models import CNNClassifier, save_model
from .utils import ConfusionMatrix, load_data, LABEL_NAMES
import torch
import torchvision
from torchvision import transforms
import torch.utils.tensorboard as tb
from os import walk


def train(args):
    from os import path
    model = CNNClassifier(input_channels=   list(map(int, args.input_channels.split(' '))),
                          block_channels=   list(map(int, args.block_channels.split(' '))),
                          residual_channels=list(map(int, args.residual_channels.split(' '))),
                          input_kernel_size=args.input_Ksize,
                          input_stride=args.input_stride,
                          block_kernel_size=args.block_Ksize,
                          block_stride=args.block_stride,
                          residual_kernel_size=args.residual_Ksize,
                          residual_stride=args.residual_stride)
    if args.continue_training != None:
        model.load_state_dict(torch.load(path.join(path.dirname(path.abspath(__file__)), args.continue_training)))

    train_logger, valid_logger = None, None
    folder_nums = [-1]
    if args.log_dir is not None:
        folder_nums = list(map(int, list(map(lambda x: x.split('-')[0], list(walk(args.log_dir))[0][1]))))
        folder_nums.sort()
        folder_nums.insert(0, -1)
        if args.description != '': description = '/' + str(folder_nums[len(folder_nums)-1] + 1) + '-' + args.description
        else: description = '/' + str(folder_nums[-1] + 1)
        train_logger = tb.SummaryWriter(path.join((args.log_dir + description), 'train'), flush_secs=1)
        valid_logger = tb.SummaryWriter(path.join((args.log_dir + description), 'valid'), flush_secs=1)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    best_valid_accuracy = 0

    if args.continue_training:
        model.load_state_dict(torch.load(path.join(path.dirname(path.abspath(__file__)), 'cnn.th')))

    train_augmentations = None
    if args.data_aug:
        train_augmentations = transforms.Compose([
            transforms.ColorJitter(()),
            transforms.RandomHorizontalFlip(),
            #transforms.RandomResizedCrop(64),
            transforms.ToTensor()
        ])
    train_dl = load_data(args.training_path, batch_size=args.batch_size, transform=train_augmentations)
    valid_dl = load_data(args.validation_path, batch_size=args.batch_size)
    loss_func = torch.nn.CrossEntropyLoss()
    if args.adam:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)
    else:
        optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay, momentum=0.9)
    if args.step_schedule: scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', patience=10)

    global_step = 0
    for i in range(args.epochs):
        total_loss = 0
        train_accuracy = torch.tensor([],dtype=torch.bool).to(device)
        valid_accuracy = torch.tensor([],dtype=torch.bool).to(device)
        model.train()
        for j, data in enumerate(train_dl):
            input, gold = data

            input = input.to(device)
            gold = gold.to(device)
            optimizer.zero_grad()
            loss = loss_func(model(input).view([-1,6]).to(device), gold)
            loss.backward()
            optimizer.step()

            if args.log_dir is not None:
                train_logger.add_scalar('loss', loss, global_step)

            total_loss += loss.item()
            train_accuracy = torch.cat((train_accuracy, model.classify(input) == gold))
            global_step += 1

        model.eval()
        for j, data in enumerate(valid_dl):
            input, gold = data
            valid_accuracy = torch.cat((valid_accuracy, model.classify(input.to(device)) == gold.to(device)))

        train_accuracy = train_accuracy.type(torch.float).mean().item() * 100
        valid_accuracy = valid_accuracy.type(torch.float).mean().item() * 100
        if args.step_schedule:
            scheduler.step(valid_accuracy)
            train_logger.add_scalar('learning rate', optimizer.param_groups[0]['lr'], global_step)

        if args.log_dir is not None:
            train_logger.add_scalar('accuracy', train_accuracy, global_step)
            valid_logger.add_scalar('accuracy', valid_accuracy, global_step)
        else: print("Epoch: {:3d} -- Training Loss: {:10.6f} | Training Accuracy {:7.3f}% | Validation Accuracy {:7.3f}%".format(i,loss.item(),train_accuracy, valid_accuracy))

        if args.checkpoints and valid_accuracy > best_valid_accuracy:
            best_valid_accuracy = valid_accuracy
            save_model(model, str(folder_nums[-1]+1))

    print("\t-----------------------------------------------------------------------------")
    print("\t|  Option Descriptions  |       Option Strings       |     Default Values    ")
    print("\t-----------------------------------------------------------------------------")
    print("\t|Epochs:                | '-e', '--epochs'           |  ", args.epochs)
    print("\t|Input Channels Sizes:  | '-i', '--input_channels'   |  ", args.input_channels)
    print("\t|Block Channels Sizes:  | '-c', '--block_channels'   |  ", args.block_channels)
    print("\t|Residual Sizes:        | '-C', '--residual_channels'|  ", args.residual_channels)
    print("\t|Learning Rate:         | '-l', '--learning_rate'    |  ", args.learning_rate)
    print("\t|Weight Decay:          | '-w', '--weight_decay'     |  ", args.weight_decay)
    print("\t|Batch Size:            | '-b', '--batch_size'       |  ", args.batch_size)
    print("\t|Input Kernal Size:     | '-k', '--input_Ksize'      |  ", args.input_Ksize)
    print("\t|Block Kernal Size:     | '-s', '--input_stride'     |  ", args.input_stride)
    print("\t|Input Stride:          | '-K', '--block_Ksize'      |  ", args.block_Ksize)
    print("\t|Block Stride:          | '-S', '--block_stride'     |  ", args.block_stride)
    print("\t|Residual Stride:       | '-r', '--residual_Ksize'   |  ", args.residual_Ksize)
    print("\t|Residual Stride:       | '-R', '--residual_stride'  |  ", args.residual_stride)
    print("\t|Training path:         | '--training_path'          |  ", args.training_path)
    print("\t|Validation path:       | '--validation_path'        |  ", args.validation_path)
    print("\t|Step Schedule:         | '--step_schedule'          |  ", args.step_schedule)
    print("\t|Description:           | '--description'            |  ", args.description)
    print("\t|No Save:               | '--no_save'                |  ", args.no_save)
    print("\t|Continue Training:     | '--continue_training'      |  ", args.continue_training)
    print("\t|Checkpoints:           | '--checkpoints'            |  ", args.checkpoints)
    print("\t|Data Augmentation:     | '--data_aug'               |  ", args.data_aug)
    print("\t|Adam Optimizer:        | '--adam'                   |  ", args.adam)
    print("\t-----------------------------------------------------------------------------")
    print("Saved: ", not args.no_save)
    if not args.no_save: save_model(model)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument('--log_dir')
    # Put custom arguments here
    # IMPLEMENT MOMENTUM -p
    # IMPLEMENT LR STEPPING
    parser.add_argument('-e', '--epochs', type=int, default=600)
    parser.add_argument('-i', '--input_channels', type=str, default="8")
    parser.add_argument('-c', '--block_channels', type=str, default="16 24 32")
    parser.add_argument('-C', '--residual_channels', type=str, default="40 64")
    parser.add_argument('-l', '--learning_rate', type=float, default=1e-3)
    parser.add_argument('-w', '--weight_decay', type=float, default=1e-4)
    parser.add_argument('-b', '--batch_size', type=int, default=200)
    parser.add_argument('-k', '--input_Ksize', type=int, default=5)
    parser.add_argument('-s', '--input_stride', type=int, default=2)
    parser.add_argument('-K', '--block_Ksize', type=int, default=5)
    parser.add_argument('-S', '--block_stride', type=int, default=1)
    parser.add_argument('-r', '--residual_Ksize', type=int, default=5)
    parser.add_argument('-R', '--residual_stride', type=int, default=1)
    parser.add_argument('--training_path', type=str, default='data/train/')
    parser.add_argument('--validation_path', type=str, default='data/valid/')
    parser.add_argument('--step_schedule', action='store_true', default=False)
    parser.add_argument('--description', type=str, default='')
    parser.add_argument('--no_save', action='store_true', default=False)
    parser.add_argument('--continue_training')
    parser.add_argument('--checkpoints', action='store_true', default=False)
    parser.add_argument('--data_aug', action='store_true', default=False)
    parser.add_argument('--adam', action='store_true', default=False)
    parser.add_argument('-h', '--help', action='store_true', default=False)

    args = parser.parse_args()

    if args.help:
        print("\t-----------------------------------------------------------------------------")
        print("\t|  Option Descriptions  |       Option Strings       |     Default Values    ")
        print("\t-----------------------------------------------------------------------------")
        print("\t|Epochs:                | '-e', '--epochs'           |  ", args.epochs)
        print("\t|Input Channels Sizes:  | '-i', '--input_channels'   |  ", args.input_channels)
        print("\t|Block Channels Sizes:  | '-c', '--block_channels'   |  ", args.block_channels)
        print("\t|Residual Sizes:        | '-C', '--residual_channels'|  ", args.residual_channels)
        print("\t|Learning Rate:         | '-l', '--learning_rate'    |  ", args.learning_rate)
        print("\t|Weight Decay:          | '-w', '--weight_decay'     |  ", args.weight_decay)
        print("\t|Batch Size:            | '-b', '--batch_size'       |  ", args.batch_size)
        print("\t|Input Kernal Size:     | '-k', '--input_Ksize'      |  ", args.input_Ksize)
        print("\t|Block Kernal Size:     | '-s', '--input_stride'     |  ", args.input_stride)
        print("\t|Input Stride:          | '-K', '--block_Ksize'      |  ", args.block_Ksize)
        print("\t|Block Stride:          | '-S', '--block_stride'     |  ", args.block_stride)
        print("\t|Residual Stride:       | '-r', '--residual_Ksize'   |  ", args.residual_Ksize)
        print("\t|Residual Stride:       | '-R', '--residual_stride'  |  ", args.residual_stride)
        print("\t|Training path:         | '--training_path'          |  ", args.training_path)
        print("\t|Validation path:       | '--validation_path'        |  ", args.validation_path)
        print("\t|Step Schedule:         | '--step_schedule'          |  ", args.step_schedule)
        print("\t|Description:           | '--description'            |  ", args.description)
        print("\t|No Save:               | '--no_save'                |  ", args.no_save)
        print("\t|Continue Training:     | '--continue_training'      |  ", args.continue_training)
        print("\t|Checkpoints:           | '--checkpoints'            |  ", args.checkpoints)
        print("\t|Data Augmentation:     | '--data_aug'               |  ", args.data_aug)
        print("\t|Adam Optimizer:        | '--adam'                   |  ", args.adam)
        print("\t-----------------------------------------------------------------------------")
        exit()

    train(args)

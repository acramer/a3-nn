import torch
import torch.nn as nn
import torch.nn.functional as F


class CNNClassifier(torch.nn.Module):
    class Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            self.c = nn.Sequential(
                nn.Conv2d( in_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=stride),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
            )
            #self.c0 = nn.Conv2d( in_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            #self.c1 = nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            #self.c2 = nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            #self.b0 = nn.BatchNorm2d(out_channels)
            #self.b1 = nn.BatchNorm2d(out_channels)
            #self.b2 = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU()
        
        def forward(self, x):
            #y = self.relu(self.b0(self.c0(x)))
            #y = self.relu(self.b1(self.c1(y)))
            #y = self.relu(self.b2(self.c2(y)))
            y = self.c(x)
            return y


    class R_Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            #self.c0 = nn.Conv2d( in_channels, in_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            self.c0 = nn.Conv2d( in_channels, in_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            #self.c0 = nn.Conv2d( in_channels, in_channels//2, kernel_size=1, stride=1, bias=False)
            #self.c1 = nn.Conv2d( in_channels, in_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            #self.c1 = nn.Conv2d( in_channels//2, in_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            self.c2 = nn.Conv2d( in_channels//2, out_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            #self.c2 = nn.Conv2d( in_channels//2, out_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            self.c3 = nn.Conv2d( out_channels//2, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            #self.c3 = nn.Conv2d( out_channels//2, out_channels, kernel_size=1, stride=1, bias=False)
            #self.b0 = nn.BatchNorm2d(in_channels)
            self.b0 = nn.BatchNorm2d(in_channels//2)
            #self.b1 = nn.BatchNorm2d(in_channels//2)
            self.b2 = nn.BatchNorm2d(out_channels//2)
            self.b3 = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU()

            self.i3 = nn.Identity()
            if in_channels != out_channels or stride != 1:
                self.i3 = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride)
        
        def forward(self, x):
            g3 = self.i3(x)
            y = self.relu(self.b0(self.c0(x)))
            #y = self.relu(self.b1(self.c1(y)))
            y = self.relu(self.b2(self.c2(y)))
            y = self.relu(self.b3(self.c3(y) + g3))
            return y


    def __init__(self, input_channels=[8], block_channels=[16, 24], residual_channels=[32, 40, 64], num_channels=3, num_classes=6, input_kernel_size=5, input_stride=1, block_kernel_size=5, block_stride=1, residual_kernel_size=5, residual_stride=1):
        super().__init__()

        ichannels = input_channels.copy()
        last_channel = input_channels.pop(0)
        layers = [
            nn.Conv2d(num_channels, last_channel, kernel_size=input_kernel_size, padding=input_kernel_size//2, stride=input_stride),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
        ]
        for c in ichannels:
            layers.append(nn.Conv2d(last_channel, c, kernel_size=input_kernel_size, padding=input_kernel_size//2, stride=1))
            layers.append(nn.ReLU())
            last_channel = c
        
        for c in block_channels:
            layers.append(self.Block(last_channel, c, kernel_size=block_kernel_size, stride=block_stride))
            last_channel = c

        for c in residual_channels:
            layers.append(self.R_Block(last_channel, c, kernel_size=residual_kernel_size, stride=residual_stride))
            last_channel = c

        self.network = nn.Sequential(*layers)
        self.classifier = torch.nn.Linear(last_channel, num_classes)

    def forward(self, x):
        y = self.network(x)
        y = y.mean(dim=[2,3])
        y = self.classifier(y)
        return y

    def classify(self, x):
        y = self.forward(x)
        return torch.argmax(y, dim=1)



class FCN(torch.nn.Module):
    # Convolutional Block
    class Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            self.c = nn.Sequential(
                nn.Conv2d( in_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=stride),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
            )
            self.relu = nn.ReLU()
        
        def forward(self, x):
            y = self.c(x)
            return y

    # Residual Block
    class R_Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            self.c0 = nn.Conv2d( in_channels, in_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            self.c2 = nn.Conv2d( in_channels//2, out_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            self.c3 = nn.Conv2d( out_channels//2, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            self.b0 = nn.BatchNorm2d(in_channels//2)
            self.b2 = nn.BatchNorm2d(out_channels//2)
            self.b3 = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU()

            self.i3 = nn.Identity()
            if in_channels != out_channels or stride != 1:
                self.i3 = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride)
        
        def forward(self, x):
            g3 = self.i3(x)
            y = self.relu(self.b0(self.c0(x)))
            y = self.relu(self.b2(self.c2(y)))
            y = self.relu(self.b3(self.c3(y) + g3))
            return y

    # Dense Block
    class D_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 32, 16, 8]):
            super().__init__()
            channels=[8, 16, 32, 48, 32, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(         sum(c[:1]), c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(         sum(c[:2]), c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(         sum(c[:3]), c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x))) # 3 ->  8
            y1 = self.relu(self.bc1(self.c1(y))) # 8 -> 16
            y=torch.cat((y, y1),dim=1)
            y2 = self.relu(self.bc2(self.c2(y))) #16 -> 32
            y=torch.cat((y, y2), dim=1)
            y3 = self.relu(self.bc3(self.c3(y))) #32 -> 48
            y=torch.cat((y, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y))) #48 -> 32 
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))  #32 -> 16 #64 -> 16
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))  #16 ->  8 #32 ->  8
            y=torch.cat((y, y6), dim=1)
            y = self.relu(self.c7(y))  # 8 ->  6 #16 ->  6
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y
            
    # Experimental Block
    class E_Block(nn.Module):
        def __init__(self, channels=[8, 40, 80, 200, 80, 40, 8]):
            super().__init__()
            channels=[8, 16, 32, 48, 32, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(               c[0], c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(               c[1], c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(               c[2], c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x))) # 3 ->  8
            y1 = self.relu(self.bc1(self.c1(y))) # 8 -> 16
            y2 = self.relu(self.bc2(self.c2(y1))) #16 -> 32
            y3 = self.relu(self.bc3(self.c3(y2))) #32 -> 48
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y))) #48 -> 32 
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))  #32 -> 16 #64 -> 16
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))  #16 ->  8 #32 ->  8
            y=torch.cat((y, y6), dim=1)
            y = self.relu(self.c7(y))  # 8 ->  6 #16 ->  6
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    # Experimental Block #2
    class E2_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 32, 16, 8]):
            super().__init__()
            #channels=[8, 16, 24, 32, 40, 48, 40, 32, 24, 16, 8]
            channels=[8, 16, 24, 40, 56, 40, 24, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.bc7 = nn.BatchNorm2d(c[7])
            self.bc8 = nn.BatchNorm2d(c[8])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(               c[0], c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(               c[1], c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(               c[2], c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), c[7],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c8 = nn.ConvTranspose2d(sum(c[:8]), c[8],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c11 = nn.ConvTranspose2d(sum(c[:9]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x)))
            y1 = self.relu(self.bc1(self.c1(y)))
            y2 = self.relu(self.bc2(self.c2(y1)))
            y3 = self.relu(self.bc3(self.c3(y2)))
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y)))
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))
            y=torch.cat((y, y6), dim=1)
            y7 = self.relu(self.bc7(self.c7(y)))
            y=torch.cat((y, y7), dim=1)
            y8 = self.relu(self.bc8(self.c8(y)))
            y=torch.cat((y, y8), dim=1)
            y = self.relu(self.c11(y))
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    # Experimental Block #3
    class E3_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 64, 48, 32, 16, 8]):
            super().__init__()
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.bc7 = nn.BatchNorm2d(c[7])
            self.bc8 = nn.BatchNorm2d(c[8])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = FCN.Block(c[0], c[1])
            self.c2 = FCN.Block(c[1], c[2])
            self.c3 = FCN.Block(c[2], c[3])
            self.c4 = FCN.R_Block(       sum(c[:4]), c[4])
            self.c5 = FCN.R_Block(       sum(c[:5]), c[5])
            self.c6 = FCN.R_Block(       sum(c[:6]), c[6])
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), c[7],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c8 = nn.ConvTranspose2d(sum(c[:8]), c[8],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c9 = nn.ConvTranspose2d(sum(c[:9]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x)))
            y1 = self.relu(self.bc1(self.c1(y)))
            y2 = self.relu(self.bc2(self.c2(y1)))
            y3 = self.relu(self.bc3(self.c3(y2)))
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y)))
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))
            y=torch.cat((y, y6), dim=1)
            y7 = self.relu(self.bc7(self.c7(y)))
            y=torch.cat((y, y7), dim=1)
            y8 = self.relu(self.bc8(self.c8(y)))
            y=torch.cat((y, y8), dim=1)
            y = self.relu(self.c9(y))
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    def __init__(self, channels=[9, 18, 36, 72, 36, 18, 9]):
        super().__init__()
        """
        Your code here.
        Hint: The FCN can be a bit smaller the the CNNClassifier since you need to run it at a higher resolution
        Hint: Use up-convolutions
        Hint: Use skip connections
        Hint: Use residual connections
        Hint: Always pad by kernel_size / 2, use an odd kernel_size
        """
        self.net = self.E3_Block()

    def forward(self, x):
        """
        Your code here
        @x: torch.Tensor((B,3,H,W))
        @return: torch.Tensor((B,6,H,W))
        Hint: Apply input normalization inside the network, to make sure it is applied in the grader
        Hint: Input and output resolutions need to match, use output_padding in up-convolutions, crop the output
              if required (use z = z[:, :, :H, :W], where H and W are the height and width of a corresponding strided
              convolution
        """
        return self.net(x)


model_factory = {
    'cnn': CNNClassifier,
    'fcn': FCN,
}


def save_model(model, des=''):
    from torch import save
    from os import path
    for n, m in model_factory.items():
        if isinstance(model, m):
            return save(model.state_dict(), path.join(path.dirname(path.abspath(__file__)), '%s.th' % (n+des)))
    raise ValueError("model type '%s' not supported!" % str(type(model)))


def load_model(model):
    from torch import load
    from os import path
    r = model_factory[model]()
    r.load_state_dict(load(path.join(path.dirname(path.abspath(__file__)), '%s.th' % model), map_location='cpu'))
    return r
